from selenium import webdriver
from selenium.webdriver.common.by import By
import time, math
def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))

try:
  link="https://SunInJuly.github.io/execute_script.html"
  browser = webdriver.Chrome()
  browser.get(link)

  x_element = browser.find_element(By.ID, "input_value")
  # time.sleep(10)

  # print(x_element.text)

  x = x_element.text
  y = calc(x)

  radio_b = browser.find_element(By.ID, "robotCheckbox").click()

  input = browser.find_element(By.ID, "answer").send_keys(y)

  button = browser.find_element(By.CSS_SELECTOR, "button.btn")
  browser.execute_script("return arguments[0].scrollIntoView(true);", button)
  check_b = browser.find_element(By.ID, "robotsRule").click()
  button.click()

finally:
  time.sleep(30)
  browser.quit()

