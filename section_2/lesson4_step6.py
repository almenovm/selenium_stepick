from selenium import webdriver
from selenium.webdriver.common.by import By

browser = webdriver.Chrome()
browser.get("http://suninjuly.github.io/cats.html")

# говорим WebDriver искать каждый элемент в течение 5 секунд
browser.implicitly_wait(5)

button = browser.find_element(By.ID, "button")
button.click()

