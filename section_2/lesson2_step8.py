import os
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

try:
    link = "http://suninjuly.github.io/file_input.html"
    browser = webdriver.Chrome()
    browser.get(link)

    input1 = browser.find_element(By.NAME, "firstname")
    input1.send_keys("Madiyar")
    input2 = browser.find_element(By.NAME, "lastname")
    input2.send_keys("Uzumaki")
    input3 = browser.find_element(By.NAME, "email")
    input3.send_keys("maduz@gmail.com")

    file_input = browser.find_element(By.ID, "file")
    cur_dir = os.path.abspath(os.path.dirname(__file__))
    file_path = os.path.join(cur_dir, 'file.txt')
    file_input.send_keys(file_path)
    button = browser.find_element(By.TAG_NAME, "button").click()

finally:
    time.sleep(30)
    browser.quit()
