from selenium import webdriver
from selenium.webdriver.common.by import By
import time


link = ""
browser = webdriver.Chrome()
browser.get(link)

# allert классический
alert = browser.switch_to.alert
alert.accept()

# confirm с выбоором принятия или отмены
confirm = browser.switch_to.alert
confirm.accept()
confirm.dismiss()

# promt с полем ввода
prompt = browser.switch_to.alert
prompt.send_keys("My answer")
prompt.accept()


