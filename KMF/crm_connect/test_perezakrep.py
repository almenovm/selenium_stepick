import time
import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
link = "https://crm-dev.kmf.kz/auth"

@pytest.fixture(scope="function")
def browser():
    print("\nstart browser for test..")
    browser = webdriver.Chrome()
    yield browser
    print("\nquit browser..")
    browser.quit()


class TestClientsPage1():

    def test_guest_should_see_login_link(self, browser):
        browser.get(link)
        browser.implicitly_wait(5)

        email = browser.find_element(By.CSS_SELECTOR, "[selenide = 'sql4OTtXV-auth-input-email']")
        email.send_keys("madiyar.almenov@kmf.kz")
        password = browser.find_element(By.CSS_SELECTOR, "[selenide = 'sql4OTtXV-auth-input-password']")
        password.send_keys("12MadikO12@")
        login = browser.find_element(By.TAG_NAME, "button")
        login.click()

        all_clients = browser.find_element(By.CSS_SELECTOR, "[selenide = 'menu-item-41ROY5Y5Il Все Клиенты']")
        all_clients.click()
        browser.find_element(By.ID, "mat-checkbox-3-input").click()

# 10    def test_guest_should_press_all_clients_on_the_main_page(self, browser):
#
#         browser.find_element(By.CSS_SELECTOR, ".basket-mini .btn-group > a")
#