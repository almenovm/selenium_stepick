from selenium import webdriver
from selenium.webdriver.common.by import By
import time

try:
    link = "http://suninjuly.github.io/registration1.html"
    browser = webdriver.Chrome()
    browser.get(link)

    # elements = browser.find_elements(By.TAG_NAME, "input")
    #
    # for element in elements:
    #     element.send_keys("Madik")

    name = browser.find_element(By.CSS_SELECTOR, "input:required.first")
    last_name = browser.find_element(By.CLASS_NAME, "second:required")
    email = browser.find_element(By.CLASS_NAME, "third:required")

    name.send_keys("Madiyar")
    last_name.send_keys("Uchiha")
    email.send_keys("madi@gmail.com")

    button = browser.find_element(By.CSS_SELECTOR, "button.btn")
    time.sleep(5)
    button.click()

    time.sleep(1)

    welcome_text_elt = browser.find_element(By.TAG_NAME, "h1")
    welcome_text = welcome_text_elt.text

    assert "Congratulations! You have successfully registered!" == welcome_text

finally:
    time.sleep(10)
    browser.quit()
