from selenium import webdriver
from selenium.webdriver.common.by import By
import time

browser = webdriver.Chrome()
browser.get("https://www.degreesymbol.net/")

# мы хотим найти ссылку с текстом "Degree symbol in Math" и перейти по ней. Если хотим найти элемент по полному соответствию текста
# link = browser.find_element(By.LINK_TEXT, "Degree Symbol in Math")

# А если хотим найти элемент со ссылкой по подстроке, то нужно написать следующий код:
link = browser.find_element(By.PARTIAL_LINK_TEXT, "Math")

link.click()

time.sleep(30)
browser.quit()
